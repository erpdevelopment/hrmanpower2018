package com.oracle.mbcgroup.view;

import oracle.jbo.domain.Number;

public class UserHelper {
    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getUserName() {
        return userName;
    }

    public void setPersonId(Number personId) {
        this.personId = personId;
    }

    public Number getPersonId() {
        return personId;
    }

    public void setEmployeeNumber(String employeeNumber) {
        this.employeeNumber = employeeNumber;
    }

    public String getEmployeeNumber() {
        return employeeNumber;
    }

    public void setEmployeeName(String employeeName) {
        this.employeeName = employeeName;
    }

    public String getEmployeeName() {
        return employeeName;
    }

    public void setOrganizationId(Number organizationId) {
        this.organizationId = organizationId;
    }

    public Number getOrganizationId() {
        return organizationId;
    }

    public void setOrganizationName(String organizationName) {
        this.organizationName = organizationName;
    }

    public String getOrganizationName() {
        return organizationName;
    }

    public void setDirectorateName(String directorateName) {
        this.directorateName = directorateName;
    }

    public String getDirectorateName() {
        return directorateName;
    }

    public void setMgrPersonId(Number mgrPersonId) {
        this.mgrPersonId = mgrPersonId;
    }

    public Number getMgrPersonId() {
        return mgrPersonId;
    }

    public void setMgrName(String mgrName) {
        this.mgrName = mgrName;
    }

    public String getMgrName() {
        return mgrName;
    }

    public void setMgrOrganizationId(Number mgrOrganizationId) {
        this.mgrOrganizationId = mgrOrganizationId;
    }

    public Number getMgrOrganizationId() {
        return mgrOrganizationId;
    }
    private String userName;
    private Number personId;
    private String employeeNumber;
    private String employeeName;
    private Number organizationId;
    private String organizationName;
    private String directorateName;
    private Number mgrPersonId;
    private String mgrName;
    private Number mgrOrganizationId;
    private Number notificationCount;
    private Number approvedNtfCount;
    private Number rejectedNtfCount;
    private String roleName;
    //added for Secuirty purposes to make salary viewed by Admin (Afshin )and Cherrie only
    private String viewSalary;


    public void setRoleName(String roleName) {
        this.roleName = roleName;
    }

    public String getRoleName() {
        return roleName;
    }

    public void setNotificationCount(Number notificationCount) {
        this.notificationCount = notificationCount;
    }

    public Number getNotificationCount() {
        if (notificationCount == null)
            notificationCount = new Number(0);
        return notificationCount;
    }

    public UserHelper() {
        super();
    }

    public void setApprovedNtfCount(Number approvedNtfCount) {
        this.approvedNtfCount = approvedNtfCount;
    }

    public Number getApprovedNtfCount() {
        return approvedNtfCount;
    }

    public void setRejectedNtfCount(Number rejectedNtfCount) {
        this.rejectedNtfCount = rejectedNtfCount;
    }

    public Number getRejectedNtfCount() {
        return rejectedNtfCount;
    }


    public void setViewSalary(String viewSalary) {
        this.viewSalary = viewSalary;
    }

    public String getViewSalary() {
        return viewSalary;
    }
}
