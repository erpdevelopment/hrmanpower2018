package com.oracle.mbcgroup.view;

import java.io.IOException;
import java.io.InputStream;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.event.ValueChangeEvent;

import oracle.adf.model.BindingContext;
import oracle.adf.model.binding.DCBindingContainer;
import oracle.adf.model.binding.DCIteratorBinding;
import oracle.adf.view.rich.component.rich.data.RichTable;
import oracle.adf.view.rich.context.AdfFacesContext;

import oracle.binding.BindingContainer;
import oracle.binding.OperationBinding;

import org.apache.myfaces.trinidad.model.UploadedFile;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

public class UploadExcelBean {
    public UploadExcelBean() {
        super();
    }
    private RichTable empTable;
    private RichTable empNewTable;
    public void uploadFileVCE(ValueChangeEvent valueChangeEvent)  {
        UploadedFile file = (UploadedFile) valueChangeEvent.getNewValue();

        try {
            //Check if file is XLSX
            if (file.getContentType().equalsIgnoreCase("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet") ||
                file.getContentType().equalsIgnoreCase("application/xlsx")) {

                readNProcessExcelx(file.getInputStream()); //for xlsx
                FacesMessage msg = new FacesMessage("Vacancies Updloaded Successfully ..");
                                          msg.setSeverity(FacesMessage.SEVERITY_WARN);
                                          FacesContext.getCurrentInstance().addMessage(null, msg);

            }
            //Check if file is XLS
            else if (file.getContentType().equalsIgnoreCase("application/vnd.ms-excel")) {

                if (file.getFilename().toUpperCase().endsWith(".XLS")) {
                   
                    readNProcessExcel(file.getInputStream()); //for xls
                    FacesMessage msg = new FacesMessage("Vacancies Updloaded Successfully ..");
                                              msg.setSeverity(FacesMessage.SEVERITY_WARN);
                                              FacesContext.getCurrentInstance().addMessage(null, msg);
                }

            } else {
                FacesMessage msg = new FacesMessage("File format not supported.-- Upload XLS or XLSX file");
                msg.setSeverity(FacesMessage.SEVERITY_WARN);
                FacesContext.getCurrentInstance().addMessage(null, msg);
            }
            AdfFacesContext.getCurrentInstance().addPartialTarget(empNewTable);

        } catch (IOException e) {
            // TODO
        }
    }
    
        public BindingContainer getBindings() {
            return BindingContext.getCurrent().getCurrentBindingsEntry();
        }
    public void readNProcessExcelx(InputStream xlsx) throws IOException {


           BindingContext bindingctx = BindingContext.getCurrent();
           BindingContainer bindingcnt = getBindings();
           DCBindingContainer bindingImple = (DCBindingContainer) bindingcnt;
           DCIteratorBinding iter = bindingImple.findIteratorBinding("XxhrBudgetVacVO1Iterator");
       //Use XSSFWorkbook for XLS file
        XSSFWorkbook WorkBook = null;
        int sheetIndex = 0;
    oracle.jbo.domain.Number BudgetId= getCurrentBudgetID();
        try {
            WorkBook = new XSSFWorkbook(xlsx);
        } catch (IOException e) {

        }
        XSSFSheet sheet = WorkBook.getSheetAt(sheetIndex);
        Integer skipRw = 1;
        Integer skipcnt = 1;
        for (Row tempRow : sheet) {
               
                {
            if (skipcnt > skipRw) {
          executeOperation("CreateInsert").execute();
               
             oracle.jbo.Row row = iter.getNavigatableRowIterator().getCurrentRow();
         
                for (int column = 0; column < 6; column++)
                {

         Cell MytempCell = tempRow.getCell(column);
                  if (MytempCell != null) 
                  {
                     
            //  System.out.println(MytempCell.getStringCellValue());
                        
        if (column == 0) {
     
          row.setAttribute("JobName", MytempCell.getStringCellValue());
                        
        }
        else  if (column == 1) {
                       
        row.setAttribute("Company", MytempCell.getStringCellValue());
                                        
        }
       
       else  if (column == 2) {
                                       
                        row.setAttribute("Directorate", MytempCell.getStringCellValue());
                                                        
                        }
                        else  if (column == 3) {
                                                        
        row.setAttribute("Location", MytempCell.getStringCellValue());
                                                                         
                                         }
                        else  if (column == 4) {
                                                        
              row.setAttribute("VacancyType", "Replacement");
                                                                         
                                         }
                        else  if (column == 5) {
                                                        
                        row.setAttribute("StartDate", MytempCell.getDateCellValue());
                                                                         
                                         }
                  
                    }
                    // If cell is null
                    else {
                       // System.out.println("Empty");
                       if (column == 4) {
                             row.setAttribute("VacancyType", "Budgeted");                                    
                                       
                                                                                                
                                                                }
                      
                    }

                }
                row.setAttribute("IsUploaded", "Y");
                row.setAttribute("FkBudgetId", BudgetId);
            }
            }
            skipcnt++;
        }
        // Save Data 
        Commit_action();
        
        //Refresh Table
        ExecuteBudgetDirectorate_action();
      
   
    }
    
    public void readNProcessExcel(InputStream xls) throws IOException {

        BindingContext bindingctx = BindingContext.getCurrent();
        BindingContainer bindingcnt = getBindings();
        DCBindingContainer bindingImple = (DCBindingContainer) bindingcnt;
        DCIteratorBinding iter = bindingImple.findIteratorBinding("XxhrBudgetVacVO1Iterator");

        //Use HSSFWorkbook for XLS file
        HSSFWorkbook WorkBook = null;

        int sheetIndex = 0;

        try {
            WorkBook = new HSSFWorkbook(xls);
        } catch (IOException e) {
            System.out.println("Exception : " + e);
        }

        HSSFSheet sheet = WorkBook.getSheetAt(sheetIndex);
        oracle.jbo.domain.Number BudgetId= getCurrentBudgetID();
        Integer skipRw = 1;
        Integer skipcnt = 1;
        Integer sno = 1;
        //Iterate over excel rows
        for (Row tempRow : sheet) {
            System.out.println(skipcnt + "--" + skipRw);
            if (skipcnt > skipRw) { //skip first n row for labels.
                //Create new row in table
                executeOperation("CreateInsert").execute();
                //Get current row from iterator
                oracle.jbo.Row row = iter.getNavigatableRowIterator().getCurrentRow();
                int Index = 0;
                //Iterate over row's columns
                for (int column = 0; column < 6; column++) {
                    Cell MytempCell = tempRow.getCell(column);
                    if (MytempCell != null) 
                    {
                        
                        System.out.println(MytempCell.getStringCellValue());
                                  
                        if (column == 0) {
                        
                        row.setAttribute("JobName", MytempCell.getStringCellValue());
                                  
                        }
                        else  if (column == 1) {
                                 
                        row.setAttribute("Company", MytempCell.getStringCellValue());
                                                  
                        }
                        
                        else  if (column == 2) {
                                                 
                                  row.setAttribute("Directorate", MytempCell.getStringCellValue());
                                                                  
                                  }
                                  else  if (column == 3) {
                                                                  
                        row.setAttribute("Location", MytempCell.getStringCellValue());
                                                                                   
                                                   }
                                  else  if (column == 4) {
                                                                  
                        row.setAttribute("VacancyType", "Replacement");
                                                                                   
                                                   }
                        
                        else  if (column == 3) {
                                                        
                        row.setAttribute("StartDate", MytempCell.getDateCellValue());
                                                                         
                                         }
                        
                        
                        
                        
                       // Index = MytempCell.getColumnIndex();
                    }
                    
                    // If cell is null
                    else {
                       // System.out.println("Empty");
                       if (column == 4) {
                             row.setAttribute("VacancyType", "Budgeted");                                    
                                       
                                                                                                
                        }
                      
                    }
           
                }
                
                row.setAttribute("IsUploaded", "Y");
                row.setAttribute("FkBudgetId", BudgetId);
                sno++;
            }
            skipcnt++;
        }
        // Save Data 
        Commit_action();
        
        //Refresh Table
        ExecuteBudgetDirectorate_action();
    
    }
    
    
    
    public void updateProcessExcel(InputStream xls) throws IOException {


        //Use HSSFWorkbook for XLS file
        HSSFWorkbook WorkBook = null;

        int sheetIndex = 0;

        try {
            WorkBook = new HSSFWorkbook(xls);
        } catch (IOException e) {
            System.out.println("Exception : " + e);
        }

        HSSFSheet sheet = WorkBook.getSheetAt(sheetIndex);
        oracle.jbo.domain.Number BudgetId= getCurrentBudgetID();
        Integer skipRw = 1;
        Integer skipcnt = 1;
        Integer sno = 1;
      String vacid="";
        String loc ="";
        String postion ="";
        String comp="";
        String Dept="";
        String currentstatus="";
            String BusinessRemarks="";
            String reopenforbudgetyear="";
        //Iterate over excel rows
        for (Row tempRow : sheet) {
        // System.out.println(skipcnt + "--" + skipRw);
            if (skipcnt > skipRw) { //skip first n row for labels.
               
            
                int Index = 0;
                //Iterate over row's columns
                for (int column = 0; column < 8; column++) {
                    Cell MytempCell = tempRow.getCell(column);
                    if (MytempCell != null) 
                    {
                       
                        if (column == 0) {
                           
                      vacid= String.valueOf((Number)MytempCell.getNumericCellValue());
                          //Double.toString(MytempCell.getNumericCellValue());
                            System.out.println(vacid);          
                        }
                        else  if (column == 1) {
                                 
                       postion= MytempCell.getStringCellValue();
                                                  
                        }
                        
                        else  if (column == 2) {
                                                 
                              comp= MytempCell.getStringCellValue();
                                                                  
                                  }
                                  else  if (column == 3) {
                                                                  
                     Dept= MytempCell.getStringCellValue();
                                                                                   
                                                   }
                                  else  if (column == 4) {
                                                                  
                      loc=MytempCell.getStringCellValue();
                                                                                   
                                                   }
                        
                        else  if (column == 5) {
                                                        
                        BusinessRemarks=MytempCell.getStringCellValue();
                                                                         
                                         }
                        else  if (column == 6) {
                                                        
                        reopenforbudgetyear=MytempCell.getStringCellValue();
                                                                         
                                         }
                        else  if (column == 7) {
                                                        
                        currentstatus=MytempCell.getStringCellValue();
                                                                         
                                         }
                        
                        
                        
                        
                      
                    }
                    
                    // If cell is null
                    else {
                        if (column == 0) {
                        
                        vacid="NULL";
                                  
                        }
                        else  if (column == 1) {
                                 
                        postion= "";
                                                  
                        }
                        
                        else  if (column == 2) {
                                                 
                              comp="";
                                                                  
                                  }
                                  else  if (column == 3) {
                                                                  
                        Dept= "";
                                                                                   
                                                   }
                                  else  if (column == 4) {
                                                                  
                        loc="";
                                                                                   
                        }
                        else  if (column == 5) {
                                                        
                        BusinessRemarks="";
                                                                         
                                         }
                        else  if (column == 6) {
                                                        
                        reopenforbudgetyear="";
                                                                         
                                         }
                        else  if (column == 7) {
                                                        
                        currentstatus="";
                                                                         
                                         }
                                                       

                      
                    }
                 
           
                } //End loop throug columns
              
                sno++;
                if(!vacid.equals("NULL"))
                
                {
                        updateVacancyupload_action(vacid,postion,comp,Dept,loc,BusinessRemarks,reopenforbudgetyear,currentstatus);
                    
                    }
            } //skip row 1 
            skipcnt++;
        }//loop through rows
        // Save Data 
        Commit_action();
        
        //Refresh Table
        ExecuteBudgetDirectorate_action();
       
    }
    
    public void updateProcessStaffListExcel(InputStream xls) throws IOException {


        //Use HSSFWorkbook for XLS file
        HSSFWorkbook WorkBook = null;

        int sheetIndex = 0;

        try {
            WorkBook = new HSSFWorkbook(xls);
        } catch (IOException e) {
            System.out.println("Exception : " + e);
        }

        HSSFSheet sheet = WorkBook.getSheetAt(sheetIndex);
        oracle.jbo.domain.Number BudgetId= getCurrentBudgetID();
        Integer skipRw = 1;
        Integer skipcnt = 1;
        Integer sno = 1;
      String empnum="";
        String hrbudget ="";
        String remarks ="";
      
        //Iterate over excel rows
        for (Row tempRow : sheet) {
        // System.out.println(skipcnt + "--" + skipRw);
            if (skipcnt > skipRw) { //skip first n row for labels.
               
            
                int Index = 0;
                //Iterate over row's columns
                for (int column = 0; column < 5; column++) {
                    Cell MytempCell = tempRow.getCell(column);
                    if (MytempCell != null) 
                    {
                       
                        if (column == 0) {
                           
                      empnum= String.valueOf((Number)MytempCell.getNumericCellValue());
                          //Double.toString(MytempCell.getNumericCellValue());
                            System.out.println(empnum);          
                        }
                      
                                  else  if (column == 3) {
                                                                  
                     hrbudget= MytempCell.getStringCellValue();
                                                                                   
                                                   }
                                  else  if (column == 4) {
                                                                  
                      remarks=MytempCell.getStringCellValue();
                                                                                   
                                                   }
                        
                        
                        
                        
                        
                        
                      
                    }
                    
                    // If cell is null
                    else {
                        if (column == 0) {
                        
                        empnum="NULL";
                                  
                        }
                     
                                  else  if (column == 3) {
                                                                  
                        hrbudget= "";
                                                                                   
                                                   }
                                  else  if (column == 4) {
                                                                  
                        remarks="";
                                                                                   
                        }
                                                       

                      
                    }
              
           
                } //End loop throug columns
                
                              sno++;
                if(!empnum.equals("NULL"))
                {
                this.updatecurrentstaffload_action(empnum, hrbudget, remarks);
                   
                    
                    }
            } //skip row 1 
            skipcnt++;
        }//loop through rows
        // Save Data 
        Commit_action();
        
        //Refresh Table
        ExecuteBudgetDirectorate_action();
       
    }
    public void updateNProcessExcelx(InputStream xlsx) throws IOException {


        
       //Use XSSFWorkbook for XLS file
        XSSFWorkbook WorkBook = null;
        int sheetIndex = 0;
        String vacid="";
          String loc ="";
          String currentstatus="";
          String BusinessRemarks="";
          String reopenforbudgetyear="";
          String postion ="";
          String comp="";
          String Dept="";
        try {
            WorkBook = new XSSFWorkbook(xlsx);
        } catch (IOException e) {

        }
        XSSFSheet sheet = WorkBook.getSheetAt(sheetIndex);
        Integer skipRw = 1;
        Integer skipcnt = 1;
        for (Row tempRow : sheet) {
               
                {
            if (skipcnt > skipRw) {
        
         
                for (int column = 0; column < 8; column++)
                {

               Cell MytempCell = tempRow.getCell(column);
                    if (MytempCell != null) 
                    {
                       
                        if (column == 0) {
                           
                      vacid= String.valueOf((Number)MytempCell.getNumericCellValue());
                          //Double.toString(MytempCell.getNumericCellValue());
                            System.out.println(vacid);          
                        }
                        else  if (column == 1) {
                                 
                       postion= MytempCell.getStringCellValue();
                                                  
                        }
                        
                        else  if (column == 2) {
                                                 
                              comp= MytempCell.getStringCellValue();
                                                                  
                                  }
                                  else  if (column == 3) {
                                                                  
                     Dept= MytempCell.getStringCellValue();
                                                                                   
                                                   }
                                  else  if (column == 4) {
                                                                  
                      loc=MytempCell.getStringCellValue();
                                                                                   
                                                   }
                        else  if (column == 5) {
                                                        
                        BusinessRemarks=MytempCell.getStringCellValue();
                                                                         
                                         }
                        else  if (column == 6) {
                                                        
                        reopenforbudgetyear=MytempCell.getStringCellValue();
                                                                         
                                         }
                        else  if (column == 7) {
                                                        
                        currentstatus=MytempCell.getStringCellValue();
                                                                         
                                         }
                  
                    }
                    // If cell is null
                    else {
                        if (column == 0) {
                        
                        vacid="NULL";
                                  
                        }
                        else  if (column == 1) {
                                 
                        postion= "";
                                                  
                        }
                        
                        else  if (column == 2) {
                                                 
                              comp="";
                                                                  
                                  }
                                  else  if (column == 3) {
                                                                  
                        Dept= "";
                                                                                   
                                                   }
                                  else  if (column == 4) {
                                                                  
                        loc="";
                                                                                   
                        }
                        else  if (column == 5) {
                                                        
                        BusinessRemarks="";
                                                                         
                                         }
                        else  if (column == 6) {
                                                        
                        reopenforbudgetyear="";
                                                                         
                                         }
                        else  if (column == 7) {
                                                        
                        currentstatus="";
                                                                         
                                         }
                                                       

                      
                    }// if cell null
                    
                
                    

                }// end loop through columns
                if(!vacid.equals("NULL"))
                
                {
                        updateVacancyupload_action(vacid,postion,comp,Dept,loc,BusinessRemarks,reopenforbudgetyear,currentstatus);
                    
                    }
            } // end skip row 1
            } //end loop through rows
            skipcnt++;
        }
        // Save Data 
        Commit_action();
        
        //Refresh Table
        ExecuteBudgetDirectorate_action();
     
    
    }

   
    public void updateProcessStaffListExcelx(InputStream xlsx) throws IOException {


        
       //Use XSSFWorkbook for XLS file
        XSSFWorkbook WorkBook = null;
        int sheetIndex = 0;
        String empnum="";
          String hrbudget ="";
          String remarks ="";
         
        try {
            WorkBook = new XSSFWorkbook(xlsx);
        } catch (IOException e) {

        }
        XSSFSheet sheet = WorkBook.getSheetAt(sheetIndex);
        Integer skipRw = 1;
        Integer skipcnt = 1;
        for (Row tempRow : sheet) {
               
                {
            if (skipcnt > skipRw) {
        
         
                for (int column = 0; column < 5; column++)
                {

               Cell MytempCell = tempRow.getCell(column);
                    if (MytempCell != null) 
                    {
                       
                        if (column == 0) {
                           
                      empnum= String.valueOf((Number)MytempCell.getNumericCellValue());
                          //Double.toString(MytempCell.getNumericCellValue());
                            System.out.println(empnum);          
                        }
//                      
  
                      else     if (column == 3) {
                          try
                          {
                     hrbudget= MytempCell.getStringCellValue();
                      System.out.println(hrbudget); 
                          }
                          catch(IllegalStateException ex)
                                                                       
                          {
                                  hrbudget= String.valueOf((Number)MytempCell.getNumericCellValue());
                                   System.out.println(hrbudget);  
                              }            
                                                   }
                                  else  if (column == 4) {
                                                 try
                                                 {
                      remarks=MytempCell.getStringCellValue();
                                                       System.out.println(remarks); 
                                                       
                                                 }
                                                       catch(IllegalStateException ex)
                                                                                                    
                                                       {
                                                              remarks= String.valueOf((Number)MytempCell.getNumericCellValue());
                                                                System.out.println(hrbudget);  
                                                           } 
                                                   }
                  
                    }
                    // If cell is null
                    else {
                        if (column == 0) {
                        
                        empnum="NULL";
                                  
                        }
                     
                                  else  if (column == 3) {
                                                                  
                        hrbudget= "";
                                                                                   
                                                   }
                                  else  if (column == 4) {
                                                                  
                        remarks="";
                                                                                   
                        }
                                                       

                      
                    }// if cell null
                    
                 
                    

                }// end loop through columns
                if(!empnum.equals("NULL"))
                
                {
                    
                    this.updatecurrentstaffload_action(empnum, hrbudget, remarks);
                  
                    
                    }
            } // end skip row 1
            } //end loop through rows
            skipcnt++;
        }
        // Save Data 
        Commit_action();
        
        //Refresh Table
        ExecuteBudgetDirectorate_action();
     
    
    }
    
    public BindingContainer getBindingsCont() {
        return BindingContext.getCurrent().getCurrentBindingsEntry();
    }
    public OperationBinding executeOperation(String operation) {
        OperationBinding createParam = getBindingsCont().getOperationBinding(operation);
        return createParam;

    }

    public void setEmpTable(RichTable empTable) {
        this.empTable = empTable;
    }

    public RichTable getEmpTable() {
        return empTable;
    }


    public String Commit_action() {
        BindingContainer bindings = getBindings();
        OperationBinding operationBinding = bindings.getOperationBinding("Commit");
        Object result = operationBinding.execute();
        if (!operationBinding.getErrors().isEmpty()) {
            return null;
        }
        return null;
    }
    public String deleteVacanies_action() {
        BindingContainer bindings = getBindings();
        OperationBinding operationBinding = bindings.getOperationBinding("deleteVacancies");
        operationBinding.execute();
        if (!operationBinding.getErrors().isEmpty()) {
            return null;
        }
        Commit_action();
        ExecuteBudgetDirectorate_action();
        FacesMessage msg = new FacesMessage("Vacancies Deleted Successfully ..");
                                  msg.setSeverity(FacesMessage.SEVERITY_WARN);
                                  FacesContext.getCurrentInstance().addMessage(null, msg);
        return null;
    }

    public String ExecuteBudgetDirectorate_action() {
        BindingContainer bindings = getBindings();
        OperationBinding operationBinding = bindings.getOperationBinding("initBudgetDirectorate");
        Object result = operationBinding.execute();
        if (!operationBinding.getErrors().isEmpty()) {
            return null;
        }
        return null;
    }
    public oracle.jbo.domain.Number getCurrentBudgetID()
    {
        
            BindingContainer bindings = getBindings();
            OperationBinding operationBinding2 = bindings.getOperationBinding("getCurrentBudgetId");
            Object result2 = operationBinding2.execute();
          return  (oracle.jbo.domain.Number)result2;
        }
    
    
    
    public String updateVacancyupload_action( String vacid  , String job , String company, String directorate , String location ,String businessremark,String reopenbudget,String currentstatus ) {
        // Add event code here...
    
                BindingContainer bindings = getBindings();
        OperationBinding oper = bindings.getOperationBinding("updateVacancyUpload"); 
        oper.getParamsMap().put("Pvacid", vacid);
        oper.getParamsMap().put("pjob", job);
        oper.getParamsMap().put("pcomapny", company);
        oper.getParamsMap().put("pdirectorate", directorate);
        oper.getParamsMap().put("plocation", location);
        oper.getParamsMap().put("pbusinessremark", businessremark);
        oper.getParamsMap().put("preopenbudget", reopenbudget);
        oper.getParamsMap().put("pcurrentstatus", currentstatus);
       oper.execute();
        if (!oper.getErrors().isEmpty()) {
            
            return null;
        }
        else
        {
            

            return null;
            
            
            }
    
    }
    public String updatecurrentstaffload_action( String empnum ,String budgetenitity ,String Remarks ) {
        // Add event code here...
    
          BindingContainer bindings = getBindings();
        OperationBinding oper = bindings.getOperationBinding("UpdateStaffList"); 
        oper.getParamsMap().put("Pempnum", empnum);
        oper.getParamsMap().put("pbudgetenitity", budgetenitity);
        oper.getParamsMap().put("pRemarks", Remarks);
     
       oper.execute();
        if (!oper.getErrors().isEmpty()) {
            
            return null;
        }
        else
        {
            

            return null;
            
            
            }
    
    }
    
    
    public void updateFileVCE(ValueChangeEvent valueChangeEvent)  {
        UploadedFile file = (UploadedFile) valueChangeEvent.getNewValue();

        try {
            //Check if file is XLSX
            if (file.getContentType().equalsIgnoreCase("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet") ||
                file.getContentType().equalsIgnoreCase("application/xlsx")) {

              //  readNProcessExcelx(file.getInputStream()); //for xlsx
              updateNProcessExcelx(file.getInputStream());
              
                FacesMessage msg = new FacesMessage("Vacancies Updated Successfully ..");
                                          msg.setSeverity(FacesMessage.SEVERITY_WARN);
                                          FacesContext.getCurrentInstance().addMessage(null, msg);

            }
            //Check if file is XLS
            else if (file.getContentType().equalsIgnoreCase("application/vnd.ms-excel")) {

                if (file.getFilename().toUpperCase().endsWith(".XLS")) {
                    updateProcessExcel(file.getInputStream());
                    FacesMessage msg = new FacesMessage("Vacancies Updated Successfully ..");
                                              msg.setSeverity(FacesMessage.SEVERITY_WARN);
                                              FacesContext.getCurrentInstance().addMessage(null, msg);
                }

            } else {
                FacesMessage msg = new FacesMessage("File format not supported.-- Upload XLS or XLSX file");
                msg.setSeverity(FacesMessage.SEVERITY_WARN);
                FacesContext.getCurrentInstance().addMessage(null, msg);
            }
            AdfFacesContext.getCurrentInstance().addPartialTarget(empTable);

        } catch (IOException e) {
            // TODO
        }
    }
    public void updatecurrentStaffListVCE(ValueChangeEvent valueChangeEvent)  {
        UploadedFile file = (UploadedFile) valueChangeEvent.getNewValue();

        try {
            //Check if file is XLSX
            if (file.getContentType().equalsIgnoreCase("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet") ||
                file.getContentType().equalsIgnoreCase("application/xlsx")) {

              //  readNProcessExcelx(file.getInputStream()); //for xlsx
            //  updateNProcessExcelx(file.getInputStream());
            updateProcessStaffListExcelx (file.getInputStream());
                FacesMessage msg = new FacesMessage("Staff List Updated Successfully ..");
                                          msg.setSeverity(FacesMessage.SEVERITY_WARN);
                                          FacesContext.getCurrentInstance().addMessage(null, msg);

            }
            //Check if file is XLS
            else if (file.getContentType().equalsIgnoreCase("application/vnd.ms-excel")) {

                if (file.getFilename().toUpperCase().endsWith(".XLS")) {
                    updateProcessStaffListExcel(file.getInputStream());
                    FacesMessage msg = new FacesMessage("Staff List Updated Successfully ..");
                                              msg.setSeverity(FacesMessage.SEVERITY_WARN);
                                              FacesContext.getCurrentInstance().addMessage(null, msg);
                }

            } else {
                FacesMessage msg = new FacesMessage("File format not supported.-- Upload XLS or XLSX file");
                msg.setSeverity(FacesMessage.SEVERITY_WARN);
                FacesContext.getCurrentInstance().addMessage(null, msg);
            }
            AdfFacesContext.getCurrentInstance().addPartialTarget(empTable);

        } catch (IOException e) {
            // TODO
        }
    }
    public String Delete_action() {
        BindingContainer bindings = getBindings();
        OperationBinding operationBinding = bindings.getOperationBinding("Delete");
        Object result = operationBinding.execute();
        if (!operationBinding.getErrors().isEmpty()) {
            return null;
        }
        OperationBinding operationBinding2 = bindings.getOperationBinding("Commit");
        Object result2 = operationBinding2.execute();
        return null;
    }


    public void setEmpNewTable(RichTable empNewTable) {
        this.empNewTable = empNewTable;
    }

    public RichTable getEmpNewTable() {
        return empNewTable;
    }

  
}

