package com.oracle.mbcgroup.view.util;

import java.io.IOException;

import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;

import oracle.adf.controller.ControllerContext;
import org.apache.commons.JSFUtils;
public class ManpowerUtil {
    public ManpowerUtil() {
        super();
    }
    public static void redirectToView(String viewId) {
        FacesContext fCtx = FacesContext.getCurrentInstance();
        ExternalContext eCtx = fCtx.getExternalContext();

        String activityUrl = ControllerContext.getInstance().getGlobalViewActivityURL(viewId);
   
        try {
            eCtx.redirect(activityUrl);
        } catch (IOException e) {
            e.printStackTrace();
            JSFUtils.addFacesErrorMessage("Exception when redirect to " + viewId);
        }
    }
}
