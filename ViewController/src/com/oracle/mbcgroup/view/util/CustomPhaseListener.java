package com.oracle.mbcgroup.view.util;

import com.oracle.mbcgroup.view.UserHelper;

import javax.faces.context.FacesContext;

import oracle.adf.controller.v2.lifecycle.Lifecycle;
import oracle.adf.controller.v2.lifecycle.PagePhaseEvent;
import oracle.adf.controller.v2.lifecycle.PagePhaseListener;

import org.apache.commons.JSFUtils;

public class CustomPhaseListener implements PagePhaseListener  {
    public CustomPhaseListener() {
        super();
    }

    @Override
    public void afterPhase(PagePhaseEvent pagePhaseEvent)  {
        // TODO Implement this method
      //  ManpowerUtil.redirectToView("login");
        if (pagePhaseEvent.getPhaseId() == Lifecycle.PREPARE_RENDER_ID) {
            String currentView = FacesContext.getCurrentInstance()
                                             .getViewRoot()
                                             .getViewId();
            if (!(currentView.endsWith("login"))) {
                UserHelper user = (UserHelper) JSFUtils.getFromSession("userHelper");
                if (user == null) {
                   
                    ManpowerUtil.redirectToView("login");
                }
            }
        }
    }

    @Override
    public void beforePhase(PagePhaseEvent pagePhaseEvent) {
        // TODO Implement this method
    }
}
