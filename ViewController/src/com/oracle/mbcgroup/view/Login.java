package com.oracle.mbcgroup.view;

import java.util.Date;
import java.util.HashMap;

import oracle.adf.view.rich.component.rich.RichDocument;
import oracle.adf.view.rich.component.rich.RichForm;
import oracle.adf.view.rich.component.rich.fragment.RichPageTemplate;
import oracle.adf.view.rich.component.rich.output.RichOutputText;

import oracle.binding.OperationBinding;

import oracle.jbo.ViewObject;
import oracle.jbo.domain.Number;
import org.apache.commons.ADFUtils;
import org.apache.commons.JSFUtils;


public class Login {
    private RichPageTemplate pt1;
    private RichForm f1;
    private RichDocument d1;
    private RichOutputText invalidMsg;
    private String userName;
    private String password;

    public void setPt1(RichPageTemplate pt1) {
        this.pt1 = pt1;
    }

    public RichPageTemplate getPt1() {
        return pt1;
    }

    public void setF1(RichForm f1) {
        this.f1 = f1;
    }

    public RichForm getF1() {
        return f1;
    }

    public void setD1(RichDocument d1) {
        this.d1 = d1;
    }

    public RichDocument getD1() {
        return d1;
    }

    public void setInvalidMsg(RichOutputText invalidMsg) {
        this.invalidMsg = invalidMsg;
    }

    public RichOutputText getInvalidMsg() {
        return invalidMsg;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getUserName() {
        return userName;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getPassword() {
        return password;
    }
    public String onClcikLoginBtn() {
        // Add event code here...
        OperationBinding oper = ADFUtils.findOperation("authenticateUser");
        oper.getParamsMap().put("userName", this.getUserName().toUpperCase());
        oper.getParamsMap().put("password", this.getPassword());
        String isAuthenticatedStr = (String) oper.execute();
        if ("Y".equals(isAuthenticatedStr)) 
        
        {   if(this.checkempauthorized().equals("Y"))
            {
            this.initSessionData();
            return "mainbudget";
            }
            else 
            this.invalidMsg.setValue("This User isnot Authorized to access MBC Hr Manpower Application,Kindly Contact System Adminstartor to Provide access.");
        } else {
            this.invalidMsg.setValue("You have specified an invalid User ID or Password. Please check and try again.");
        }

        return null;
    }
    // check employee is authorized to access the application 
    private String checkempauthorized()
    {
            OperationBinding oper = ADFUtils.findOperation("checkEmployeeAuthourized");
            oper.getParamsMap().put("userName", this.getUserName().toUpperCase());
            oper.execute();
            String result = (String) oper.getResult();
            return result;
        
        }
    //Get Logged in Employee Data
    private void initSessionData() {
        OperationBinding oper = ADFUtils.findOperation("initLogonEmployee");
        oper.getParamsMap().put("userName", this.getUserName().toUpperCase());
        oper.execute();
        HashMap employeeDataHashMap = (HashMap) oper.getResult();
        Number personId = (Number) employeeDataHashMap.get("PersonId");
        String employeeNumber = (String) employeeDataHashMap.get("EmployeeNumber");
        String employeeName = (String) employeeDataHashMap.get("FullName");
        String directorateName = (String) employeeDataHashMap.get("DirectorateName");
        Number organizationId = (Number) employeeDataHashMap.get("OrganizationId");
        String organizationName = (String) employeeDataHashMap.get("OrganizationName");
        Number positionId = (Number) employeeDataHashMap.get("PositionId");
        String positionName = (String) employeeDataHashMap.get("PositionName");
        String emailAddress = (String) employeeDataHashMap.get("EmailAddress");
        Number mgrPersonId = (Number) employeeDataHashMap.get("MgrPersonId");
        String mgrFullName = (String) employeeDataHashMap.get("MgrFullName");
        Number mgrOrganizationId = (Number) employeeDataHashMap.get("MgrOrganizationId");
        String roleName = (String) employeeDataHashMap.get("RoleName");
        String AdminBplanner="000";
       if(roleName.equals("BP"))
        AdminBplanner=directorateName;
        UserHelper userHelper = new UserHelper();
        userHelper.setUserName(this.getUserName().toUpperCase());
        userHelper.setPersonId(personId);
        userHelper.setEmployeeNumber(employeeNumber);
        userHelper.setEmployeeName(employeeName);
        userHelper.setOrganizationId(organizationId);
        userHelper.setOrganizationName(organizationName);
        userHelper.setDirectorateName(directorateName);
        userHelper.setMgrPersonId(mgrPersonId);
        userHelper.setMgrName(mgrFullName);
        userHelper.setMgrOrganizationId(mgrOrganizationId);
        userHelper.setRoleName(roleName);
        if(this.getUserName().toUpperCase().equals("AFETEMAD") ||this.getUserName().toUpperCase().equals("CHMANGULABNAN"))
         // userHelper.setViewSalary(this.getUserName().toUpperCase());
            userHelper.setViewSalary("Y");
        else userHelper.setViewSalary("N");
       // userHelper.setDparam(AdminBplanner);
//        oper=ADFUtils.findOperation("getWorklistCount");
//        oper.getParamsMap().put("personId", personId);
//        HashMap ntfMap = (HashMap) oper.execute();
//        userHelper.setApprovedNtfCount((Number) ntfMap.get("APPROVED"));
//        userHelper.setRejectedNtfCount((Number) ntfMap.get("REJECTED"));
//        userHelper.setNotificationCount((Number) ntfMap.get("OPEN"));
        // this method to store data in session and to be sure that the user access any page when he is logged through login page
        JSFUtils.storeOnSession("userHelper", userHelper);
        JSFUtils.storeOnSession("directoreparam", AdminBplanner);
//        ViewObject asWorklistVO = ADFUtils.findIterator("AsWorklistVOIterator").getViewObject();
//        asWorklistVO.setNamedWhereClauseParam("PersonIdPar", personId);
    }
}
