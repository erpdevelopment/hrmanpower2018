package com.oracle.mbcgroup.view;

import java.io.Serializable;

import java.sql.CallableStatement;
import java.sql.SQLException;

import java.util.Map;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;

import oracle.adf.controller.ControllerContext;
import oracle.adf.controller.ViewPortContext;
import oracle.adf.model.BindingContext;
import oracle.adf.model.binding.DCBindingContainer;
import oracle.adf.model.binding.DCIteratorBinding;
import oracle.adf.share.ADFContext;
import oracle.adf.view.rich.component.rich.input.RichInputNumberSlider;

import oracle.binding.BindingContainer;

import oracle.binding.OperationBinding;

import oracle.jbo.JboException;
import oracle.jbo.Row;
import oracle.jbo.RowSetIterator;
import oracle.jbo.server.ApplicationModuleImpl;

import org.apache.commons.ADFUtils;

public class MbcHrBudget implements Serializable {
    private RichInputNumberSlider incrementValue;
    private RichInputNumberSlider bonusValue;

    public void increaseBonus(ActionEvent actionEvent){
        System.out.println("Bonus = "+getBonusValue().getValue());
        System.out.println("Step 1");
            BindingContext bindingctx = BindingContext.getCurrent();
            System.out.println("Step 2");
            BindingContainer bindingcnt = bindingctx.getCurrentBindingsEntry();
            System.out.println("Step 3");
            DCBindingContainer bindingImple = (DCBindingContainer) bindingcnt;
            DCIteratorBinding header = bindingImple.findIteratorBinding("XxhrBudgetAllView1Iterator");
            DCIteratorBinding line = bindingImple.findIteratorBinding("XxhrBudgetLinesAllView1Iterator");
            System.out.println("Step 4");
            Row row  = header.getCurrentRow();
            System.out.println("Step 5");
        ApplicationModuleImpl am = (ApplicationModuleImpl) ADFUtils.getDCBindingContainer().getApplicationModule();
            CallableStatement cst = null;
            System.out.println("Step 6");
            try {
                System.out.println("Step 7");
                cst = am.getDBTransaction().createCallableStatement("begin xxhr_budget_pkg.increase_bonus(?, ?);end;", 0);
                System.out.println("Step 8");
                cst.setString(1, row.getAttribute("BudgetId").toString());
                cst.setString(2,getBonusValue().getValue().toString());
                //row.setAttribute("BonusPercent",getBonusValue().getValue());
                System.out.println("Step 9");
                am.getTransaction().commit();
                cst.executeUpdate();
                System.out.println("Step 10");
            } catch (SQLException e) {
                throw new JboException(e.getMessage());
            }
            line.executeQuery();
            am.getTransaction().commit();
        }
    public void increment(ActionEvent actionEvent){
            System.out.println("Increment = "+getIncrementValue().getValue());
            BindingContext bindingctx = BindingContext.getCurrent();
            BindingContainer bindingcnt = bindingctx.getCurrentBindingsEntry();
            DCBindingContainer bindingImple = (DCBindingContainer) bindingcnt;
            DCIteratorBinding header = bindingImple.findIteratorBinding("XxhrBudgetAllView1Iterator");
            DCIteratorBinding line = bindingImple.findIteratorBinding("XxhrBudgetLinesAllView1Iterator");
            
            Row row  = header.getCurrentRow();
            ApplicationModuleImpl am = (ApplicationModuleImpl) ADFUtils.getDCBindingContainer().getApplicationModule();
            CallableStatement cst = null;
            try {
                cst = am.getDBTransaction().createCallableStatement("begin xxhr_budget_pkg.increment_budget(?, ?);end;", 0);
                cst.setString(1, row.getAttribute("BudgetId").toString());
                cst.setString(2,getIncrementValue().getValue().toString());
                //row.setAttribute("IncrementPercent",getIncrementValue().getValue());
                am.getTransaction().commit();
                cst.executeUpdate();
            } catch (SQLException e) {
                throw new JboException(e.getMessage());
            }
            line.executeQuery();
            am.getTransaction().commit();
        }

    public void postVacancyBatch(ActionEvent actionEvent){
            BindingContext bindingctx = BindingContext.getCurrent();
            BindingContainer bindingcnt = bindingctx.getCurrentBindingsEntry();
            DCBindingContainer bindingImple = (DCBindingContainer) bindingcnt;
            DCIteratorBinding vac = bindingImple.findIteratorBinding("XxhrBudgetVacVO1Iterator");
            ApplicationModuleImpl am = (ApplicationModuleImpl) ADFUtils.getDCBindingContainer().getApplicationModule();
            CallableStatement cst = null;
            // if admin post so all vacnacies is posted /if business planner post so vacnaies of business planner directorate only posted
            Map sessionscope = ADFContext.getCurrent().getSessionScope();
            String directorate= (String) sessionscope.get("directoreparam");
          try {
           
               //  if(directorate.equals("000"))
                
                 //{ 
                cst = am.getDBTransaction().createCallableStatement("begin xxhr_budget_pkg.create_vacancy_batch;end;", 0);
                // }
//                else
//                 {
//                         cst = am.getDBTransaction().createCallableStatement("begin xxhr_budget_pkg.create_planner_vacancy_batch(?);end;", 0);
//                         cst.setString(1,directorate);
//                     
//                     }
                //row.setAttribute("IncrementPercent",getIncrementValue().getValue());
                am.getTransaction().commit();
                cst.executeUpdate();
                am.getTransaction().commit();
                vac.executeQuery();
            } 
          
          
          catch (SQLException e) {
              
              
               // throw new JboException(e.getMessage());
               FacesMessage fm =
                   new FacesMessage(FacesMessage.SEVERITY_ERROR, "Application Error",
                                    "Sql Exception: " + e.getMessage());
               
               FacesContext fctx = FacesContext.getCurrentInstance();
               fctx.addMessage("ApplicationInfo", fm);
               //fctx.renderResponse();
              
              
              
            }
        }    
    public void setIncrementValue(RichInputNumberSlider increment) {
        this.incrementValue = increment;
    }

    public RichInputNumberSlider getIncrementValue() {
        return incrementValue;
    }

    public void setBonusValue(RichInputNumberSlider bonus) {
        this.bonusValue = bonus;
    }

    public RichInputNumberSlider getBonusValue() {
        return bonusValue;
    }
    public void showErrorDialog() {
        ControllerContext context = ControllerContext.getInstance();
        ViewPortContext currentViewPort = context.getCurrentViewPort();
        if (currentViewPort.isExceptionPresent()) {
            FacesMessage fm =
                new FacesMessage(FacesMessage.SEVERITY_ERROR, "Application Error",
                                 "Controller Exception: " + currentViewPort.getExceptionData().getMessage());
            
            FacesContext fctx = FacesContext.getCurrentInstance();
            fctx.addMessage("ApplicationInfo", fm);
            fctx.renderResponse();
        }
    }

    public BindingContainer getBindings() {
        return BindingContext.getCurrent().getCurrentBindingsEntry();
    }
    public String updateAssignment_action() {
        // Add event code here...
//        BindingContext bindingctx = BindingContext.getCurrent();
//        BindingContainer bindingcnt = bindingctx.getCurrentBindingsEntry();
//        DCBindingContainer bindingImple = (DCBindingContainer) bindingcnt;
//        DCIteratorBinding budgetdist = bindingImple.findIteratorBinding("XxhrBudgetDistAllView1Iterator");
//        Row row  = budgetdist.getCurrentRow();
//        RowSetIterator rsi = budgetdist.getRowSetIterator();
//        rsi.reset();
//        Row currRow=null;
//        while(rsi.hasNext()) {
//        currRow = rsi.next();
//        if (currRow.getAttribute("PositionName").equals("attributeValue")) {
//         rsi.closeRowSetIterator();
//      //        }               
//        }
//        rsi.closeRowSetIterator();
//       FacesMessage fm =
//            new FacesMessage(FacesMessage.SEVERITY_ERROR,"Message",row.getAttribute("Segment3").toString()+"-"+row.getAttribute("PositionName").toString()+"**"+row.getAttribute("LocationCode").toString());
//        FacesContext fctx = FacesContext.getCurrentInstance();
//        fctx.addMessage("ApplicationInfo", fm);
//        fctx.renderResponse();
                BindingContainer bindings = getBindings();
        OperationBinding operationBinding = bindings.getOperationBinding("updateAssignment"); 
      Object result=   operationBinding.execute();
        if (!operationBinding.getErrors().isEmpty()) {
            
            return null;
        }
        else
        {
            
                FacesMessage fm =  new FacesMessage(FacesMessage.SEVERITY_INFO,"Message"," Assignments Updated Successfully");
                           // new FacesMessage(FacesMessage.SEVERITY_ERROR,"Message","Num Of Rows :"+result.toString());
                        FacesContext fctx = FacesContext.getCurrentInstance();
                        fctx.addMessage("ApplicationInfo", fm);
                        fctx.renderResponse(); 
            return null;
            
            
            }
    
    }
    
    
    
    
    public String revertAssignment_action() {
        // Add event code here...
    
                BindingContainer bindings = getBindings();
        OperationBinding operationBinding = bindings.getOperationBinding("revertAssignment"); 
      Object result=   operationBinding.execute();
        if (!operationBinding.getErrors().isEmpty()) {
            
            return null;
        }
        else
        {
            
                FacesMessage fm =
                            new FacesMessage(FacesMessage.SEVERITY_INFO,"Message"," Assignments Reverted Successfully");
                        FacesContext fctx = FacesContext.getCurrentInstance();
                        fctx.addMessage("ApplicationInfo", fm);
                        fctx.renderResponse(); 
            return null;
            
            
            }
    
    }

    public String refresh_dist_action() {
        BindingContainer bindings = getBindings();
        OperationBinding operationBinding = bindings.getOperationBinding("Execute");
        Object result = operationBinding.execute();
        if (!operationBinding.getErrors().isEmpty()) {
            return null;
        }
        return null;
    }
}
