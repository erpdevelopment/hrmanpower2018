package com.oracle.mbcgroup.view;


import com.oracle.mbcgroup.eview.XxhrBudgetVacVORowImpl;

import java.io.IOException;

import java.io.InputStream;

import java.util.Map;

import javax.faces.application.FacesMessage;

import javax.faces.context.FacesContext;

import javax.faces.event.ValueChangeEvent;

import oracle.adf.view.rich.event.DialogEvent;
import oracle.adf.model.BindingContext;
import oracle.adf.view.rich.component.rich.RichPopup;

import oracle.adf.view.rich.event.DialogEvent;

import oracle.adf.view.rich.event.LaunchPopupEvent;

import oracle.binding.BindingContainer;
import oracle.binding.OperationBinding;

import org.apache.myfaces.trinidad.event.DisclosureEvent;
import oracle.adf.model.BindingContext;
import oracle.adf.model.binding.DCBindingContainer;
import oracle.adf.model.binding.DCIteratorBinding;
import oracle.adf.share.ADFContext;

import oracle.adf.view.rich.component.rich.data.RichTable;
import oracle.adf.view.rich.context.AdfFacesContext;

import oracle.adfinternal.view.faces.model.binding.FacesCtrlLOVBinding;

import oracle.jbo.Row;

import org.apache.myfaces.trinidad.model.UploadedFile;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

public class Vacancies {

    private RichPopup p1;
    private RichPopup openp;
    private RichPopup closep;
    private String savebtn_txt;
    private RichTable empTable;
    public void setSavebtn_txt(String savebtn_txt) {
        this.savebtn_txt = savebtn_txt;
    }

    public String getSavebtn_txt() {
        return savebtn_txt;
    }

    public Vacancies() {
    }

    public void setOpenp(RichPopup openp) {
        this.openp = openp;
    }

    public RichPopup getOpenp() {
        return openp;
    }

    public void setClosep(RichPopup closep) {
        this.closep = closep;
    }

    public RichPopup getClosep() {
        return closep;
    }

    public void setP1(RichPopup p1) {
        this.p1 = p1;
    }

    public RichPopup getP1() {
        return p1;
    }
    public BindingContainer getBindings() {
        return BindingContext.getCurrent().getCurrentBindingsEntry();
    }

    public String AddVacancy_action() {
        // in add set varaible to text of the button to be save so in save click check if it's add add row in notification table /if edit don't add row 
        BindingContainer bindings = getBindings();
        OperationBinding operationBinding = bindings.getOperationBinding("CreateInsert");
        Object result = operationBinding.execute();
        if (!operationBinding.getErrors().isEmpty()) {
     
            //open popup
            
            return null;
        }
        
     else 
        {
            
            // Set Current Budget Id 
            BindingContext bindingctx = BindingContext.getCurrent();
            BindingContainer bindingcnt = getBindings();
            DCBindingContainer bindingImple = (DCBindingContainer) bindingcnt;
            DCIteratorBinding iter = bindingImple.findIteratorBinding("XxhrBudgetVacVO1Iterator");
            oracle.jbo.Row row = iter.getNavigatableRowIterator().getCurrentRow();
            OperationBinding operationBinding2 = bindings.getOperationBinding("getCurrentBudgetId");
            Object result2 = operationBinding2.execute();
            row.setAttribute("FkBudgetId", (oracle.jbo.domain.Number)result2);
            row.setAttribute("IsUploaded", "N");
            
            }
        this.setSavebtn_txt("Save");
        RichPopup.PopupHints hints = new RichPopup.PopupHints();
        this.getP1().show(hints);
        return null;
    }
    public String EditVacancy_action() {
        this.setSavebtn_txt("Update");
        RichPopup.PopupHints hints = new RichPopup.PopupHints();
        this.getP1().show(hints);
        return null;
    }

    public String Cancel_Vacancny() {
        BindingContainer bindings = getBindings();
        OperationBinding operationBinding = bindings.getOperationBinding("Rollback");
        Object result = operationBinding.execute();
        if (!operationBinding.getErrors().isEmpty()) {
            
         
           
            return null;
        }
        RichPopup.PopupHints hints = new RichPopup.PopupHints();
        this.getP1().hide();
        return null;
    }

    public String SaveVacancy_action() {
        BindingContainer bindings = getBindings();
        OperationBinding operationBinding = bindings.getOperationBinding("Commit");
        // Here Add Notification
        if(this.getSavebtn_txt().equals("Save"))
        {
        Add_vacancy_notification();
        }
        Object result = operationBinding.execute();
        if (!operationBinding.getErrors().isEmpty()) {
            return null;
        }
        RichPopup.PopupHints hints = new RichPopup.PopupHints();
        this.getP1().hide();
        return null;
    }

    public String Delete_action() {
        BindingContainer bindings = getBindings();
        OperationBinding operationBinding = bindings.getOperationBinding("Delete");
        Object result = operationBinding.execute();
        if (!operationBinding.getErrors().isEmpty()) {
            return null;
        }
        OperationBinding operationBinding2 = bindings.getOperationBinding("Commit");
        Object result2 = operationBinding2.execute();
        return null;
    }

    public void DiscloseVacancyTabs(DisclosureEvent disclosureEvent) {
        // Add event code here...
        BindingContainer bindings = getBindings();
        OperationBinding operationBinding = bindings.getOperationBinding("ExecuteCloseVacancy");
        Object result = operationBinding.execute();
        OperationBinding operationBinding2 = bindings.getOperationBinding("ExecuteOpenVacancy");
        Object result2 = operationBinding2.execute();
    }
    
    public void Add_vacancy_notification()
    {
        
            BindingContext bindingctx = BindingContext.getCurrent();
            BindingContainer bindingcnt = getBindings();
            DCBindingContainer bindingImple = (DCBindingContainer) bindingcnt;
            DCIteratorBinding vac = bindingImple.findIteratorBinding("XxhrBudgetVacVO1Iterator");
             XxhrBudgetVacVORowImpl row  = (XxhrBudgetVacVORowImpl)vac.getCurrentRow();
             StringBuilder  NotifcationSubject= new StringBuilder("New Vacnacy has been Added Employee Type is");
            NotifcationSubject.append(" ");
            NotifcationSubject.append(row.getEmployeeType()); 
            NotifcationSubject.append("  / ");
            NotifcationSubject.append("Directorate is ");
            NotifcationSubject.append(" ");
            NotifcationSubject.append(row.getDirectorate());
            NotifcationSubject.append("  / ");
            NotifcationSubject.append("BusinessGroup is ");
            NotifcationSubject.append(" ");
            NotifcationSubject.append(row.getBusinessGroup());
            NotifcationSubject.append(" . ");
             OperationBinding operationBinding = bindingcnt.getOperationBinding("AddNotification");
            operationBinding.getParamsMap().put("pvac_tr_id",row.getBudgetVacId());
           // operationBinding.getParamsMap().put("pcreated_by",row.getCreatedBy());
            operationBinding.getParamsMap().put("pcreatedate",row.getCreationDate());
            operationBinding.getParamsMap().put("psubject",NotifcationSubject.toString());
            operationBinding.getParamsMap().put("ptype","V");
            operationBinding.execute();
        }

//    public String CloseVacancypoup_action() {
//      
//        RichPopup.PopupHints hints = new RichPopup.PopupHints();
//        this.getClosep().show(hints);
//        return null;
//    }
    public void CloseVacancy_Dialogue_Listener(DialogEvent dialogEvent) {
        // Add event code here...
        
        if (dialogEvent.getOutcome() == DialogEvent.Outcome.ok)
        {
               
//                FacesMessage Message = new FacesMessage("Record Saved Successfully!");   
//                    Message.setSeverity(FacesMessage.SEVERITY_INFO);   
//                    FacesContext fc = FacesContext.getCurrentInstance();   
//                    fc.addMessage(null, Message); 
                 BindingContainer bindings = getBindings();
                 OperationBinding operationBinding = bindings.getOperationBinding("CloseVacancy"); 
                Object result = operationBinding.execute();
            }
//        else 
//        {
//                this.getClosep().hide();
//            }
        
    }
    public String OpenVacancypoup_action() {
      
        RichPopup.PopupHints hints = new RichPopup.PopupHints();
        this.getOpenp().show(hints);
        return null;
    }
    public void OpenVacancy_Dialogue_Listener(DialogEvent dialogEvent) {
        if (dialogEvent.getOutcome() == DialogEvent.Outcome.ok)
        {
                 BindingContainer bindings = getBindings();
                 OperationBinding operationBinding = bindings.getOperationBinding("OpenVacancy"); 
                Object result = operationBinding.execute();
            }
        else 
        {
                this.getOpenp().hide();
            }
        
    }

    public void peopleGroup_onlovlaunch(LaunchPopupEvent launchPopupEvent) {
        // Add event code here...
        
        Map sessionscope = ADFContext.getCurrent().getSessionScope();
        String directorate= (String) sessionscope.get("directoreparam");
     
        
             if(!directorate.equals("000"))
            
             { 
                 BindingContext bctx = BindingContext.getCurrent();
                 BindingContainer bindings = bctx.getCurrentBindingsEntry();
                 FacesCtrlLOVBinding lov = (FacesCtrlLOVBinding)bindings.get("PeopleGroup");
                 lov.getListIterBinding().getViewObject().setWhereClause(null);
                 lov.getListIterBinding().getViewObject().setWhereClauseParams(null);
                String whereclause="";
             whereclause="SEGMENT1='"+directorate+"'";
                 //.concat(directorate);
                  lov.getListIterBinding().getViewObject().setWhereClause(whereclause);
                 
             }
    }

    public String Copy_Vacancies_action() {
        // Add event code here...   
        // 
        BindingContainer bindings = getBindings();
        OperationBinding operationBinding = bindings.getOperationBinding("copyVacancy"); 
        operationBinding.execute();
        if (!operationBinding.getErrors().isEmpty()) {
            
            return null;
        }
        else
        {
        FacesMessage Message = new FacesMessage("Vacancy Copied Successfully!");   
                            Message.setSeverity(FacesMessage.SEVERITY_INFO);
                            FacesContext fc = FacesContext.getCurrentInstance();
                            fc.addMessage(null, Message);
            
        }
        return null;
    }
//    public void updateFileVCE(ValueChangeEvent valueChangeEvent)  {
//        UploadedFile file = (UploadedFile) valueChangeEvent.getNewValue();
//
//        try {
//            //Check if file is XLSX
//            if (file.getContentType().equalsIgnoreCase("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet") ||
//                file.getContentType().equalsIgnoreCase("application/xlsx")) {
//
//              //  readNProcessExcelx(file.getInputStream()); //for xlsx
//              updateNProcessExcelx(file.getInputStream());
//              
//                FacesMessage msg = new FacesMessage("Vacancies Updated Successfully ..");
//                                          msg.setSeverity(FacesMessage.SEVERITY_WARN);
//                                          FacesContext.getCurrentInstance().addMessage(null, msg);
//
//            }
//            //Check if file is XLS
//            else if (file.getContentType().equalsIgnoreCase("application/vnd.ms-excel")) {
//
//                if (file.getFilename().toUpperCase().endsWith(".XLS")) {
//                    updateProcessExcel(file.getInputStream());
//                    FacesMessage msg = new FacesMessage("Vacancies Updated Successfully ..");
//                                              msg.setSeverity(FacesMessage.SEVERITY_WARN);
//                                              FacesContext.getCurrentInstance().addMessage(null, msg);
//                }
//
//            } else {
//                FacesMessage msg = new FacesMessage("File format not supported.-- Upload XLS or XLSX file");
//                msg.setSeverity(FacesMessage.SEVERITY_WARN);
//                FacesContext.getCurrentInstance().addMessage(null, msg);
//            }
//            AdfFacesContext.getCurrentInstance().addPartialTarget(empTable);
//
//        } catch (IOException e) {
//            // TODO
//        }
//    }
//    public oracle.jbo.domain.Number getCurrentBudgetID()
//    {
//        
//            BindingContainer bindings = getBindings();
//            OperationBinding operationBinding2 = bindings.getOperationBinding("getCurrentBudgetId");
//            Object result2 = operationBinding2.execute();
//          return  (oracle.jbo.domain.Number)result2;
//        }
//    public void updateProcessExcel(InputStream xls) throws IOException {
//
//
//        //Use HSSFWorkbook for XLS file
//        HSSFWorkbook WorkBook = null;
//
//        int sheetIndex = 0;
//
//        try {
//            WorkBook = new HSSFWorkbook(xls);
//        } catch (IOException e) {
//            System.out.println("Exception : " + e);
//        }
//
//        HSSFSheet sheet = WorkBook.getSheetAt(sheetIndex);
//        oracle.jbo.domain.Number BudgetId= getCurrentBudgetID();
//        Integer skipRw = 1;
//        Integer skipcnt = 1;
//        Integer sno = 1;
//      String vacid="";
//        String loc ="";
//        String jb ="";
//        String comp="";
//        String Dept="";
//        //Iterate over excel rows
//        for (org.apache.poi.ss.usermodel.Row tempRow : sheet) {
//        // System.out.println(skipcnt + "--" + skipRw);
//            if (skipcnt > skipRw) { //skip first n row for labels.
//               
//            
//                int Index = 0;
//                //Iterate over row's columns
//                for (int column = 0; column < 6; column++) {
//                    Cell MytempCell = tempRow.getCell(column);
//                    if (MytempCell != null) 
//                    {
//                       
//                        if (column == 0) {
//                           
//                      vacid= String.valueOf((Number)MytempCell.getNumericCellValue());
//                          //Double.toString(MytempCell.getNumericCellValue());
//                            System.out.println(vacid);          
//                        }
//                        else  if (column == 2) {
//                                 
//                       jb= MytempCell.getStringCellValue();
//                                                  
//                        }
//                        
//                        else  if (column == 3) {
//                                                 
//                              comp= MytempCell.getStringCellValue();
//                                                                  
//                                  }
//                                  else  if (column == 4) {
//                                                                  
//                     Dept= MytempCell.getStringCellValue();
//                                                                                   
//                                                   }
//                                  else  if (column == 5) {
//                                                                  
//                      loc=MytempCell.getStringCellValue();
//                                                                                   
//                                                   }
//                        
//                        
//                        
//                        
//                        
//                        
//                      
//                    }
//                    
//                    // If cell is null
//                    else {
//                        if (column == 0) {
//                        
//                        vacid="NULL";
//                                  
//                        }
//                        else  if (column == 2) {
//                                 
//                        jb= "";
//                                                  
//                        }
//                        
//                        else  if (column == 3) {
//                                                 
//                              comp="";
//                                                                  
//                                  }
//                                  else  if (column == 4) {
//                                                                  
//                        Dept= "";
//                                                                                   
//                                                   }
//                                  else  if (column == 5) {
//                                                                  
//                        loc="";
//                                                                                   
//                        }
//                                                       
//
//                      
//                    }
//                 
//           
//                } //End loop throug columns
//              
//                sno++;
//                if(!vacid.equals("NULL"))
//                
//                {
//                        updateVacancyupload_action(vacid,jb,comp,Dept,loc);
//                    
//                    }
//            } //skip row 1 
//            skipcnt++;
//        }//loop through rows
//        // Save Data 
//        Commit_action();
//        
//        //Refresh Table
//        ExecuteBudgetDirectorate_action();
//       
//    }
//    
//    public void updateNProcessExcelx(InputStream xlsx) throws IOException {
//
//
//        
//       //Use XSSFWorkbook for XLS file
//        XSSFWorkbook WorkBook = null;
//        int sheetIndex = 0;
//        String vacid="";
//          String loc ="";
//          String jb ="";
//          String comp="";
//          String Dept="";
//        try {
//            WorkBook = new XSSFWorkbook(xlsx);
//        } catch (IOException e) {
//
//        }
//        XSSFSheet sheet = WorkBook.getSheetAt(sheetIndex);
//        Integer skipRw = 1;
//        Integer skipcnt = 1;
//        for (org.apache.poi.ss.usermodel.Row tempRow : sheet) {
//               
//                {
//            if (skipcnt > skipRw) {
//        
//         
//                for (int column = 0; column < 6; column++)
//                {
//
//               Cell MytempCell = tempRow.getCell(column);
//                    if (MytempCell != null) 
//                    {
//                       
//                        if (column == 0) {
//                           
//                      vacid= String.valueOf((Number)MytempCell.getNumericCellValue());
//                          //Double.toString(MytempCell.getNumericCellValue());
//                            System.out.println(vacid);          
//                        }
//                        else  if (column == 2) {
//                                 
//                       jb= MytempCell.getStringCellValue();
//                                                  
//                        }
//                        
//                        else  if (column == 3) {
//                                                 
//                              comp= MytempCell.getStringCellValue();
//                                                                  
//                                  }
//                                  else  if (column == 4) {
//                                                                  
//                     Dept= MytempCell.getStringCellValue();
//                                                                                   
//                                                   }
//                                  else  if (column == 5) {
//                                                                  
//                      loc=MytempCell.getStringCellValue();
//                                                                                   
//                                                   }
//                  
//                    }
//                    // If cell is null
//                    else {
//                        if (column == 0) {
//                        
//                        vacid="NULL";
//                                  
//                        }
//                        else  if (column == 2) {
//                                 
//                        jb= "";
//                                                  
//                        }
//                        
//                        else  if (column == 3) {
//                                                 
//                              comp="";
//                                                                  
//                                  }
//                                  else  if (column == 4) {
//                                                                  
//                        Dept= "";
//                                                                                   
//                                                   }
//                                  else  if (column == 5) {
//                                                                  
//                        loc="";
//                                                                                   
//                        }
//                                                       
//
//                      
//                    }// if cell null
//                    
//                
//                    
//
//                }// end loop through columns
//                if(!vacid.equals("NULL"))
//                
//                {
//                        updateVacancyupload_action(vacid,jb,comp,Dept,loc);
//                    
//                    }
//            } // end skip row 1
//            } //end loop through rows
//            skipcnt++;
//        }
//        // Save Data 
//        Commit_action();
//        
//        //Refresh Table
//        ExecuteBudgetDirectorate_action();
//     
//    
//    }
//    public String ExecuteBudgetDirectorate_action() {
//        BindingContainer bindings = getBindings();
//        OperationBinding operationBinding = bindings.getOperationBinding("initBudgetDirectorate");
//        Object result = operationBinding.execute();
//        if (!operationBinding.getErrors().isEmpty()) {
//            return null;
//        }
//        return null;
//    }
//    public String Commit_action() {
//        BindingContainer bindings = getBindings();
//        OperationBinding operationBinding = bindings.getOperationBinding("Commit");
//        Object result = operationBinding.execute();
//        if (!operationBinding.getErrors().isEmpty()) {
//            return null;
//        }
//        return null;
//    }
//    
//    public String updateVacancyupload_action( String vacid  , String job , String company, String directorate , String location  ) {
//        // Add event code here...
//    
//                BindingContainer bindings = getBindings();
//        OperationBinding oper = bindings.getOperationBinding("updateVacancyUpload"); 
//        oper.getParamsMap().put("Pvacid", vacid);
//        oper.getParamsMap().put("pjob", job);
//        oper.getParamsMap().put("pcomapny", company);
//        oper.getParamsMap().put("pdirectorate", directorate);
//        oper.getParamsMap().put("plocation", location);
//       oper.execute();
//        if (!oper.getErrors().isEmpty()) {
//            
//            return null;
//        }
//        else
//        {
//            
//
//            return null;
//            
//            
//            }
//    
//    }

//    public void setEmpTable(RichTable empTable) {
//        this.empTable = empTable;
//    }
//
//    public RichTable getEmpTable() {
//        return empTable;
//    }
}
