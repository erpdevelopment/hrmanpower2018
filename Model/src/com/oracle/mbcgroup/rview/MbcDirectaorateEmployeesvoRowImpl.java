package com.oracle.mbcgroup.rview;

import java.math.BigDecimal;

import java.sql.Timestamp;

import oracle.jbo.server.ViewRowImpl;
// ---------------------------------------------------------------------
// ---    File generated by Oracle ADF Business Components Design Time.
// ---    Tue Aug 07 14:04:24 GST 2018
// ---    Custom code may be added to this class.
// ---    Warning: Do not modify method signatures of generated methods.
// ---------------------------------------------------------------------
public class MbcDirectaorateEmployeesvoRowImpl extends ViewRowImpl {
    /**
     * AttributesEnum: generated enum for identifying attributes and accessors. DO NOT MODIFY.
     */
    public enum AttributesEnum {
        EmployeeNumber,
        FullName,
        HrBudgetingEntity,
        RemarksProposedChanges,
        Directorate,
        BudgetId,
        Persontype,
        PositionName,
        Joiningdate,
        Location,
        Workingcompany,
        Company,
        Division;
        private static AttributesEnum[] vals = null;
        private static final int firstIndex = 0;

        public int index() {
            return AttributesEnum.firstIndex() + ordinal();
        }

        public static final int firstIndex() {
            return firstIndex;
        }

        public static int count() {
            return AttributesEnum.firstIndex() + AttributesEnum.staticValues().length;
        }

        public static final AttributesEnum[] staticValues() {
            if (vals == null) {
                vals = AttributesEnum.values();
            }
            return vals;
        }
    }


    public static final int EMPLOYEENUMBER = AttributesEnum.EmployeeNumber.index();
    public static final int FULLNAME = AttributesEnum.FullName.index();
    public static final int HRBUDGETINGENTITY = AttributesEnum.HrBudgetingEntity.index();
    public static final int REMARKSPROPOSEDCHANGES = AttributesEnum.RemarksProposedChanges.index();
    public static final int DIRECTORATE = AttributesEnum.Directorate.index();
    public static final int BUDGETID = AttributesEnum.BudgetId.index();
    public static final int PERSONTYPE = AttributesEnum.Persontype.index();
    public static final int POSITIONNAME = AttributesEnum.PositionName.index();
    public static final int JOININGDATE = AttributesEnum.Joiningdate.index();
    public static final int LOCATION = AttributesEnum.Location.index();
    public static final int WORKINGCOMPANY = AttributesEnum.Workingcompany.index();
    public static final int COMPANY = AttributesEnum.Company.index();
    public static final int DIVISION = AttributesEnum.Division.index();

    /**
     * This is the default constructor (do not remove).
     */
    public MbcDirectaorateEmployeesvoRowImpl() {
    }

    /**
     * Gets the attribute value for the calculated attribute EmployeeNumber.
     * @return the EmployeeNumber
     */
    public String getEmployeeNumber() {
        return (String) getAttributeInternal(EMPLOYEENUMBER);
    }

    /**
     * Gets the attribute value for the calculated attribute FullName.
     * @return the FullName
     */
    public String getFullName() {
        return (String) getAttributeInternal(FULLNAME);
    }

    /**
     * Gets the attribute value for the calculated attribute HrBudgetingEntity.
     * @return the HrBudgetingEntity
     */
    public String getHrBudgetingEntity() {
        return (String) getAttributeInternal(HRBUDGETINGENTITY);
    }

    /**
     * Gets the attribute value for the calculated attribute RemarksProposedChanges.
     * @return the RemarksProposedChanges
     */
    public String getRemarksProposedChanges() {
        return (String) getAttributeInternal(REMARKSPROPOSEDCHANGES);
    }

    /**
     * Gets the attribute value for the calculated attribute Directorate.
     * @return the Directorate
     */
    public String getDirectorate() {
        return (String) getAttributeInternal(DIRECTORATE);
    }

    /**
     * Gets the attribute value for the calculated attribute BudgetId.
     * @return the BudgetId
     */
    public BigDecimal getBudgetId() {
        return (BigDecimal) getAttributeInternal(BUDGETID);
    }


    /**
     * Gets the attribute value for the calculated attribute Persontype.
     * @return the Persontype
     */
    public String getPersontype() {
        return (String) getAttributeInternal(PERSONTYPE);
    }

    /**
     * Gets the attribute value for the calculated attribute PositionName.
     * @return the PositionName
     */
    public String getPositionName() {
        return (String) getAttributeInternal(POSITIONNAME);
    }


    /**
     * Gets the attribute value for the calculated attribute Joiningdate.
     * @return the Joiningdate
     */
    public Timestamp getJoiningdate() {
        return (Timestamp) getAttributeInternal(JOININGDATE);
    }

    /**
     * Gets the attribute value for the calculated attribute Location.
     * @return the Location
     */
    public String getLocation() {
        return (String) getAttributeInternal(LOCATION);
    }

    /**
     * Gets the attribute value for the calculated attribute Workingcompany.
     * @return the Workingcompany
     */
    public String getWorkingcompany() {
        return (String) getAttributeInternal(WORKINGCOMPANY);
    }

    /**
     * Gets the attribute value for the calculated attribute Company.
     * @return the Company
     */
    public String getCompany() {
        return (String) getAttributeInternal(COMPANY);
    }

    /**
     * Gets the attribute value for the calculated attribute Division.
     * @return the Division
     */
    public String getDivision() {
        return (String) getAttributeInternal(DIVISION);
    }
}

